﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab5.Glowny.Contract;
using Lab5.Wyswietlacz.Contract;

namespace Lab5.Glowny.Implementation
{
    public class Główny : IGlowny
    {
        IWyswietlacz wyswietlacz;

        public Główny(IWyswietlacz wyswietlacz)
        {
            this.wyswietlacz = wyswietlacz;
        }

        public void UruchomSzczotki()
        {
            wyswietlacz.Wyswietl("Uruchamiam szczotki...");
        }

        public void UruchomNaped()
        {
            wyswietlacz.Wyswietl("Uruchamiam napęd...");
        }
    }
}
