﻿using System;
using System.Reflection;
using PK.Container;
using Lab5.Glowny.Contract;
using Lab5.Glowny.Implementation;
using Lab5.Wyswietlacz.Contract;
using Lab5.Wyswietlacz.Implementation;
using MyContainer;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IGlowny));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Główny));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IWyswietlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Wyświetlacz));

        #endregion
    }
}
