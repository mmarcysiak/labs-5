﻿using System;
using PK.Container;
using Lab5.Wyswietlacz.Contract;
using Lab5.Wyswietlacz.Implementation;
using Lab5.Glowny.Implementation;
using Lab5.Glowny.Contract;
using MyContainer;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IContainer container = new Container();

            container.Register(typeof(Główny));
            container.Register(typeof(Wyświetlacz));
            return container;
        }
    }
}
