﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using Lab5.Wyswietlacz.Contract;
using Lab5.DisplayForm;

namespace Lab5.Wyswietlacz.Implementation
{
    public class Wyświetlacz : IWyswietlacz
    {
        DisplayViewModel display;
        public Wyświetlacz()
        {
            this.display = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
                {
                    // utworzenie nowej formatki graficznej stanowiącej widok
                    var form = new Form();
                    // utworzenie modelu widoku (wzorzec MVVM)
                    var viewModel = new DisplayViewModel();
                    // przypisanie modelu do widoku
                    form.DataContext = viewModel;
                    // wyświetlenie widoku
                    form.Show();
                    // zwrócenie modelu widoku do dalszych manipulacji
                    return viewModel;
                }), null);
        }
        public void Wyswietl(string tekst)
        {
            display.Text = tekst;
            Console.WriteLine(tekst);
        }
    }
}
