﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab5.Test.Fakes
{
    interface IDependingFake
    {
        IFake Fake { get; set; }
    }
}
